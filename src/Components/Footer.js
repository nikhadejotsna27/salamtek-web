import React from 'react';
import '../../src/style.css';
const Footer = () => {

  return (
    <div >
   <footer>
  <div class="content">
    <div class="top">
    
      {/* <div class="media-icons">
        <a href="#"><i class="fab fa-facebook-f"></i></a>
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        
      </div> */}
    </div>
    <div class="link-boxes">
      <ul class="box">
        <li class="link_name">Salamtek</li>
        <li><a href="#">About Us</a></li>
        <li><a href="#">Articles </a></li>
        <li><a href="#">Hot Deals </a></li>
        <li><a href="#">FAQ’S</a></li>
        <li><a href="#">Hospital & Clinics Directory</a></li>
        <li><a href="#">Ask Dr. Salamtek</a></li>
        <li><a href="#">Webinars, Exhibitions & Seminars</a></li>

      </ul>
      <ul class="box">
        <li class="link_name">For Patients</li>
        <li><a href="#">Search for Doctors</a></li>
        <li><a href="#">Search for Hospitals</a></li>
        <li><a href="#">Search for Medicines</a></li>
        <li><a href="#">Search for Medicines</a></li>
        <li><a href="#">Electronic Health Records</a></li>
        <li><a href="#">Emergency Numbers</a></li>

      </ul>
      <ul class="box">
        <li class="link_name">More</li>
        <li><a href="#">Terms & Conditions</a></li>
        <li><a href="#">Terms & Conditions</a></li>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Booking Policy</a></li>
        <li><a href="#">Cancellation & Refund Policy</a></li>
        <li><a href="#">Have A Question?</a></li>
      </ul>
      <ul class="box">
        <li class="link_name">Follow Us On</li>
        <div class="top">
    
      <div class="media-icons">
        <a href="#"><i class="fa fa-facebook"></i></a>
        
        <a href="#"><i class="fa fa-instagram"></i></a>
        <a href="#"><i class="fa fa-twitter"></i></a>
        
      </div>
    </div>
    <li class="link_name">Contact Us</li>
        <li><a href="#"><i class="fa fa-phone"></i>&nbsp;&nbsp; 965 xxxx xxxx</a></li>
        <li><a href="#"><i class="fa fa-envelope"></i>&nbsp;&nbsp; info@salamtek.com</a></li>
      </ul>
  
    </div>
  </div>
  <div class="bottom-details">
    <div class="bottom_text">
      <span class="copyright_text">Copyright © 2021 Salamtek. All right reserved </span>
      
    </div>
  </div>
</footer>

      
    </div>
  );
};

export default Footer;