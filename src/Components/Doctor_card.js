import React from 'react';
import {data} from "../Constant/data";
import '../../src/style.css';

const Doctor_card = () => {

  return (
    <div class="card1-container1">
      {data.map((item,index)=>(
    <div class="card1">
      <header class="card1-header1 header1-img">
      <img src={item.imagedata} alt="" />
        <p class="header1-title">{item.title}</p>
        {/* <h6 class="card_title"> {item.title}</h6> */}
      </header>
      
      <div class="card1-footer">
        <button class="btn" style={{borderColor:"#0A54A6"}}>In Person</button>
        <button class="btn" style={{background:"#6DD0F7",borderColor:"#6DD0F7"}}>Video</button>
      </div>
    </div>
     ))}
      
  
      
      
  </div>
  );
};

export default Doctor_card;