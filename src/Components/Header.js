import React from 'react';
import '../../src/style.css';
import kuwait from '../assets/kuwait.png'
const Header = () => {

  return (
    <div >

      <header style={{ backgroundColor: "#0A54A6" }}>
        <div class="container-fluid">

          <div class="logo">
            <h3 style={{ color: "white" }}> Salamtek </h3>
          </div>

          <div class="fullw-nav">
            <i class="fa fa-bars"></i>
          </div>


          <div class="query">

            <ul class="media-s list-unstyled">
              <li><a href="#">My Account</a></li>
              <li><a href="#">العربية</a></li>
              <li><a href="#"><i class="fa fa-bell" aria-hidden="true"></i></a></li>

              <li><a href="#"> <i class="fa fa-cart-arrow-down" aria-hidden="true"></i></a></li>
              <li><a href="#"> <img src={kuwait} tyle={{ width: "30px" }} /></a></li>


              <li>  <select>
                <option >kuwait</option>
                <option>Arbic</option>
              </select></li>

            </ul>

            <div class="clear"></div>
          </div>
        </div>
        <div class="navbar">
          <ul class="list-unstyled">
            <li class="active"><a href="#">home</a></li>
            <li><a href="#">Doctors</a></li>
            <li><a href="#">services</a></li>
            <li><a href="#">E-Shops</a></li>
            <li><a href="#">Offers</a></li>
            <li><a href="#">Medical Directory</a></li>
            <li><a href="#">Whats’s On</a></li>

          </ul>
        </div>
      </header>



    </div>
  );
};

export default Header;