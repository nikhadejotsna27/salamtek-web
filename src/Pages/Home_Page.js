import React from 'react';
import Footer from '../Components/Footer';
import Header from '../Components/Header';
import Slider from '../Components/Slider';
import Doctor_card from '../Components/Doctor_card';
import '../../src/style.css';
import healthcare from '../assets/healthcare.jpg'

const Home_Page = () => {

  return (
    <div >
      <Header />
      <Slider />

      {/* <section class="bg-primary text-light p-5">
        <div class="container">
          <div class="d-md-flex justify-content-between align-items-center">
         

            <div class="input-group news-input">
              <input
                type="text"
                class="form-control"
                placeholder="Enter E-mail"
              />
              <button
                class="btn btn-dark btn-lg"
                type="button"
              >Button
              </button>
            </div>
          </div>
        </div>
      </section> */}





      <Doctor_card />

      <section class="p-5" id="about">
        <div class="container">
          <div class="row align-items-center justify-content-between">
            <div class="col-md">

              <a href='https://svgshare.com/s/gH1' ><img class="img-fluid " src={healthcare} title='' /></a>

            </div>
            <div class="col-md p-5">
              <h2>Join Salamtek App</h2>
              <p>
              Find out why 100s of healthcare providers, insurers and national health systems choose Salamtek to help them provide better health outcomes.
              </p>
              <br/>
              <a href="#" class="btn btn-light mt3">
                <i class="bi bi-chevron-right"></i>
                View Deals
              </a>
            </div>
          </div>
        </div>
      </section>







      {/* <section>
        <div class="container">
          <h4 class="section-title">You will enjoy our cafe if you love...</h4>
          <div class="row my-5">
            <div class="col-lg-6 col-md-12">
              <div class="image-wrap">
                <img src="https://i.picsum.photos/id/1059/7360/4912.jpg?hmac=vVWk1qyiXN_VgPhpNqFm3yl2HUPW6fHqYOjTHYO2bHQ" alt="cafe-inner" />
              </div>
            </div>
            <div class="col-lg-6 col-md-12 align-self-center">
              <div class="two-col-content">
                <h5 class="inner-title"> Homely atmosphere </h5>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip'sum has 	been the industry's standard dummy text ever since the 1500s.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip'sum has been the industry's standard dummy text ever since the 1500s.</p>
                <a href="#" class="text-capitalize pink-btn"> view more </a>
              </div>
            </div>
          </div>
         
        </div>
      </section> */}




      <Footer />

    </div>
  );
};

export default Home_Page;